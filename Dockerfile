FROM rust:latest AS builder

WORKDIR /app
COPY . /app

RUN cargo build --release


FROM gcr.io/distroless/cc

USER nonroot:nonroot

COPY --from=builder /app/target/release/rando /rando

ENTRYPOINT ["/rando"]

WORKDIR /

CMD [ "rando" ]
