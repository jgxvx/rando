use std::io::BufWriter;
use std::sync::{Arc, Mutex};

use rando::token::shared::TokenCreator;
use rando::token::{alnum, base16};
use rando::token::{TokenCreationConfig, TokenService};

#[test]
fn test_alnum() {
    let creator = Box::new(alnum::Alphanumeric::new()) as Box<dyn TokenCreator + Send + Sync>;
    let writer = Arc::new(Mutex::new(BufWriter::new(Vec::new())));
    let service = TokenService::new(writer);
    let config = TokenCreationConfig::new(64, Some(10));

    let result = service.create_tokens(Arc::new(creator), config, 1);

    assert!(result.is_ok());
}

#[test]
fn test_base16() {
    let creator = Box::new(base16::Base16::new()) as Box<dyn TokenCreator + Send + Sync>;
    let writer = Arc::new(Mutex::new(BufWriter::new(Vec::new())));
    let service = TokenService::new(writer);
    let config = TokenCreationConfig::new(64, Some(10));

    let result = service.create_tokens(Arc::new(creator), config, 1);

    assert!(result.is_ok());
}
