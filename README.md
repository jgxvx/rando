# rando

A CLI tool that provides an easy to use and uniform interface for creating different types of random strings for API keys, identifiers, passwords etc.

## Usage

```shell
$ rando alnum --count=10 --length=12
$ rando base16 --length=128 --count=10
$ rando uuid --count=1000 --threads=10
```

## Installation

### Rust Toolchain

After [installing the Rust toolchain](https://rustup.rs/), the binary can be installed using Cargo:

```shell
$ make install
$ which rando
/home/itsyou/.cargo/rando
```

### Container Image

If you don't want to install the Rust toolchain, rando can be run from a container image as well:

```shell
$ docker build -t jgxvx/rando .
$ docker run --rm jgxvx/rando base64 --length=32 --count=10

$ podman build -t docker.io/jgxvx/rando .
$ podman run --rm docker.io/jgxvx/rando base64 --length=32 --count=10
```

For further convenience, add an alias:

```shell
alias rando="docker run --rm --network=none jgxvx/rando"

alias rando="podman run --rm --network=none docker.io/jgxvx/rando"
```

## Known Issues and Caveats
* Doesn't check for duplicates. Try it out: `rando base16 --length=1 --count=32`.
* The `uuid` algo only produces v4 UUIDs.
