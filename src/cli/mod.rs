use clap::{Args, Parser, Subcommand};

#[derive(Debug, Parser)]
#[command(author, version, about, long_about = None)]
#[command(name = "rando")]
#[command(about = "Creates random strings for passwords, API keys etc.")]
pub struct Cli {
    /// Number of threads to use. Skip this for detection of available CPU capacity.
    #[arg(short = 't', long = "threads")]
    pub num_threads: Option<usize>,

    /// Path to a file to write the output to.
    #[arg(short = 'o', long = "output")]
    pub output: Option<String>,

    #[command(subcommand)]
    pub commands: Commands,
}

#[derive(Debug, Subcommand)]
pub enum Commands {
    /// Creates alphanumeric (ASCII range) strings
    #[command(arg_required_else_help = true, name = "alnum")]
    Alphanumeric(LengthAndCountArgs),

    /// Creates hexadecimal strings
    #[command(arg_required_else_help = true)]
    Base16(LengthAndCountArgs),

    /// Creates a UUID v4
    #[command(arg_required_else_help = true)]
    Uuid(UuidArgs),
}

#[derive(Debug, Args)]
pub struct LengthAndCountArgs {
    /// The desired size (length) of the random string(s)
    #[arg(required = true, short = 'l', long = "length")]
    pub length: usize,

    /// The number of strings to create
    #[arg(required = true, short = 'c', long = "count")]
    pub count: usize,
}

#[derive(Debug, Args)]
pub struct UuidArgs {
    /// The number of strings to create
    #[arg(required = true, short = 'c', long = "count")]
    pub count: usize,
}
