use crate::cli;
use crate::token::{self, alnum, base16, uuid, TokenCreationConfig};
use anyhow::Result;
use std::fs::File;
use std::io::{BufWriter, Write};
use std::sync::{Arc, Mutex};
use thiserror::Error;

////////////////////////////////////////////////////////////////////////////////
// TYPES & CONVERSIONS
////////////////////////////////////////////////////////////////////////////////

type TokenWriter = BufWriter<Box<dyn Write + Send + Sync>>;

impl From<&cli::LengthAndCountArgs> for TokenCreationConfig {
    fn from(opts: &cli::LengthAndCountArgs) -> Self {
        TokenCreationConfig::new(opts.count, Some(opts.length))
    }
}

impl From<&cli::UuidArgs> for TokenCreationConfig {
    fn from(opts: &cli::UuidArgs) -> Self {
        TokenCreationConfig::new(opts.count, None)
    }
}

////////////////////////////////////////////////////////////////////////////////
// APPLICATION
////////////////////////////////////////////////////////////////////////////////

pub struct Application {
    cli_opts: cli::Cli,
}

impl Application {
    pub fn new(cli_opts: cli::Cli) -> Self {
        Self { cli_opts }
    }

    pub fn run(&self) -> Result<()> {
        let creator: token::Creator;
        let config: TokenCreationConfig;

        match &self.cli_opts.commands {
            cli::Commands::Alphanumeric(cfg) => {
                config = cfg.into();
                creator = Box::new(alnum::Alphanumeric::new());
            }
            cli::Commands::Base16(cfg) => {
                config = cfg.into();
                creator = Box::new(base16::Base16::new());
            }
            cli::Commands::Uuid(cfg) => {
                config = cfg.into();
                creator = Box::new(uuid::Uuid::new());
            }
        }

        let service = token::TokenService::new(self.create_writer());

        match service.create_tokens(Arc::new(creator), config, self.get_num_threads()) {
            Ok(_) => Ok(()),
            Err(e) => Err(ApplicationError::new(e.to_string()).into()),
        }
    }

    fn create_writer(&self) -> Arc<Mutex<TokenWriter>> {
        let writer: TokenWriter = match self.cli_opts.output {
            Some(ref path) => {
                BufWriter::new(Box::new(File::create(path).expect("Invalid output file")))
            }
            None => BufWriter::new(Box::new(std::io::stdout())),
        };

        Arc::new(Mutex::new(writer))
    }

    fn get_num_threads(&self) -> usize {
        match self.cli_opts.num_threads {
            Some(num_threads) => std::cmp::min(num_threads, num_cpus::get()),
            None => 1,
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
// APPLICATION ERRORS
////////////////////////////////////////////////////////////////////////////////

#[derive(Error, Debug, Clone)]
#[error("{msg}")]
pub struct ApplicationError {
    pub msg: String,
}

impl ApplicationError {
    pub fn new<M: Into<String>>(msg: M) -> Self {
        Self { msg: msg.into() }
    }
}

////////////////////////////////////////////////////////////////////////////////
// UNIT TESTS
////////////////////////////////////////////////////////////////////////////////

#[cfg(test)]
mod tests {
    use super::*;
    use clap::Parser;

    #[test]
    fn error_message_is_correct() {
        let e = ApplicationError::new("Test");
        assert_eq!("Test", e.to_string());
    }

    #[test]
    fn application_runs_without_error_alnum() {
        let args = vec![
            "rando",
            "--threads=1",
            "--output=/dev/null",
            "alnum",
            "--count=10",
            "--length=64",
        ];
        let opts = cli::Cli::parse_from(args);
        let app = Application::new(opts);

        let result = app.run();

        assert!(result.is_ok());
    }

    #[test]
    fn application_runs_without_error_base16() {
        let args = vec![
            "rando",
            "--threads=1",
            "--output=/dev/null",
            "base16",
            "--count=10",
            "--length=64",
        ];
        let opts = cli::Cli::parse_from(args);
        let app = Application::new(opts);

        let result = app.run();

        assert!(result.is_ok());
    }
}
