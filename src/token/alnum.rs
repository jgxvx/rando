use crate::token::{shared, TokenCreationConfig};
use anyhow::Result;
use rand::distributions;
use rand::{thread_rng, Rng};

pub struct Alphanumeric {}

impl shared::TokenCreator for Alphanumeric {
    fn create_token(&self, config: &TokenCreationConfig) -> Result<String> {
        let bin: Vec<u8> = thread_rng()
            .sample_iter(&distributions::Alphanumeric)
            .take(config.length.unwrap())
            .collect();

        Ok(String::from_utf8(bin)?)
    }
}

impl Default for Alphanumeric {
    fn default() -> Self {
        Self::new()
    }
}

impl Alphanumeric {
    pub fn new() -> Self {
        Self {}
    }
}

#[cfg(test)]
mod tests {
    use crate::token::shared::TokenCreator;

    use super::*;
    use regex::Regex;

    #[test]
    fn test_len_is_correct() {
        let c = Alphanumeric::new();

        for i in 8..=64 {
            let config = TokenCreationConfig::new(1, Some(i));
            assert_eq!(i, c.create_token(&config).unwrap().chars().count());
        }
    }

    #[test]
    fn test_form_is_correct() {
        let re = Regex::new(r"^[[:alnum:]]+$").unwrap();
        let c = Alphanumeric::new();
        let config = TokenCreationConfig::new(1, Some(256));

        assert!(re.is_match(&c.create_token(&config).unwrap()));
    }
}
