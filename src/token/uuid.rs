use crate::token::{shared, TokenCreationConfig};
use anyhow::Result;

pub struct Uuid {}

impl shared::TokenCreator for Uuid {
    fn create_token(&self, _config: &TokenCreationConfig) -> Result<String> {
        let uuid = uuid::Uuid::new_v4();

        Ok(uuid.to_string())
    }
}

impl Default for Uuid {
    fn default() -> Self {
        Self::new()
    }
}

impl Uuid {
    pub fn new() -> Self {
        Self {}
    }
}

#[cfg(test)]
mod tests {
    use crate::token::shared::TokenCreator;

    use super::*;
    use regex::Regex;

    #[test]
    fn test_form_is_correct() {
        let re = Regex::new(r"^[a-f\d]+-[a-f\d]+-[a-f\d]+-[a-f\d]+-[a-f\d]+$").unwrap();
        let c = Uuid::new();
        let config = TokenCreationConfig::new(1, None);

        assert!(re.is_match(&c.create_token(&config).unwrap()));
    }
}
