use crate::token::shared::TokenCreator;
use std::io::Write;
use std::sync::Arc;
use std::sync::Mutex;
use std::thread;

pub mod alnum;
pub mod base16;
pub mod shared;
pub mod uuid;

////////////////////////////////////////////////////////////////////////////////
// TYPES & CONVERSIONS
////////////////////////////////////////////////////////////////////////////////

pub type Creator = Box<dyn TokenCreator + Send + Sync>;

////////////////////////////////////////////////////////////////////////////////
// TOKEN CREATOR CONFIG
////////////////////////////////////////////////////////////////////////////////

#[derive(Clone, Copy)]
pub struct TokenCreationConfig {
    count: usize,
    length: Option<usize>,
}

impl TokenCreationConfig {
    pub fn new(count: usize, length: Option<usize>) -> Self {
        Self { count, length }
    }
}

////////////////////////////////////////////////////////////////////////////////
// TOKEN SERVICE
////////////////////////////////////////////////////////////////////////////////

/// Delegates the creation of pseudo-random tokens to
/// a concrete implementation of [`TokenCreator`](crate::token::shared::TokenCreator)
/// and ensures the proper amount of tokens is created.
pub struct TokenService<W: 'static + Write + Send + Sync> {
    writer: Arc<Mutex<W>>,
}

impl<W> TokenService<W>
where
    W: 'static + Write + Send + Sync,
{
    /// Creates a new instance of `TokenService`
    pub fn new(writer: Arc<Mutex<W>>) -> Self {
        TokenService { writer }
    }

    /// Creates the specified amount of tokens using the provided implementation of
    /// [`TokenCreator`](crate::token::shared::TokenCreator).
    pub fn create_tokens(
        &self,
        creator: Arc<Creator>,
        config: TokenCreationConfig,
        requested_num_threads: usize,
    ) -> anyhow::Result<()> {
        let count = config.count;
        let num_threads = self.calculate_num_threads(count, requested_num_threads);
        let default_chunk_size: usize = count / num_threads;
        let remainder: usize = count % num_threads;
        let mut thread_handles = vec![];
        let mut thread_chunk_size: usize;

        for i in 0..num_threads {
            let creator_for_thread = creator.clone();
            let writer_for_thread = self.writer.clone();

            thread_chunk_size = default_chunk_size;

            if i == num_threads - 1 {
                thread_chunk_size += remainder;
            }

            thread_handles.push(thread::spawn(move || {
                let mut tokens = String::new();
                let mut tokens_pushed: usize = 0;
                let max_tokens_pushed: usize = 100;

                for _ in 0..thread_chunk_size {
                    let token = match creator_for_thread.create_token(&config) {
                        Ok(string) => string,
                        Err(_) => String::from(""),
                    };

                    tokens += &token;
                    tokens.push('\n');
                    tokens_pushed += 1;

                    if tokens_pushed == max_tokens_pushed {
                        match write!(writer_for_thread.lock().unwrap(), "{}", tokens) {
                            Ok(_) => {
                                tokens_pushed = 0;
                                tokens.clear();
                            }
                            Err(e) => {
                                eprintln!("{}", e);
                            }
                        }
                    }
                }

                match write!(writer_for_thread.lock().unwrap(), "{}", tokens) {
                    Ok(_) => (),
                    Err(e) => {
                        eprintln!("{}", e);
                    }
                }
            }));
        }

        for thread_handle in thread_handles {
            thread_handle.join().unwrap();
        }

        self.writer.lock().unwrap().flush()?;

        Ok(())
    }

    fn calculate_num_threads(&self, count: usize, num_threads: usize) -> usize {
        std::cmp::min(count, num_threads)
    }
}
