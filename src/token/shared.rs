use crate::token::TokenCreationConfig;
use anyhow::Result;
use thiserror::Error;

/// The `TokenCreator` trait is implemented by all concrete token creators,
/// e.g. [`Base64`](crate::token::base64::Base64).
///
pub trait TokenCreator {
    /// Creates one pseudo-random token of length `len`.
    fn create_token(&self, config: &TokenCreationConfig) -> Result<String>;
}

/// All implementations of [`TokenCreator`](TokenCreator) will return
/// errors of type `TokenCreationError`.
#[derive(Error, Debug, Clone)]
#[error("{msg}")]
pub struct TokenCreationError {
    pub msg: String,
}

impl TokenCreationError {
    pub fn new<M: Into<String>>(msg: M) -> Self {
        Self { msg: msg.into() }
    }
}
