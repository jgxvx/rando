use crate::token::shared::TokenCreationError;
use crate::token::{shared, TokenCreationConfig};
use anyhow::Result;
use rand::distributions::Alphanumeric;
use rand::{thread_rng, Rng};

pub struct Base16 {}

impl shared::TokenCreator for Base16 {
    fn create_token(&self, config: &TokenCreationConfig) -> Result<String> {
        let len = config.length.unwrap();
        let take = if len % 2 == 0 { len } else { len + 1 };

        let bin: Vec<u8> = thread_rng()
            .sample_iter(&Alphanumeric)
            .take(take / 2)
            .collect();

        let mut buffer = vec![0u8; take];

        match binascii::bin2hex(&bin, &mut buffer) {
            Ok(_) => (),
            Err(_) => {
                return Err(TokenCreationError::new(
                    "Failed to convert random bytes from bin to hex",
                )
                .into());
            }
        }

        let mut string = match String::from_utf8(buffer) {
            Ok(string) => string,
            Err(e) => {
                return Err(TokenCreationError::new(e.to_string()).into());
            }
        };

        if take > len {
            string = string.chars().take(len).collect();
        }

        Ok(string)
    }
}

impl Default for Base16 {
    fn default() -> Self {
        Self::new()
    }
}

impl Base16 {
    pub fn new() -> Self {
        Self {}
    }
}

#[cfg(test)]
mod tests {
    use crate::token::shared::TokenCreator;

    use super::*;
    use regex::Regex;

    #[test]
    fn test_len_is_correct() {
        let c = Base16::new();

        for i in 1..=32 {
            let config = TokenCreationConfig::new(1, Some(i));
            assert_eq!(i, c.create_token(&config).unwrap().chars().count());
        }
    }

    #[test]
    fn test_form_is_correct() {
        let re = Regex::new(r"^[a-f\d]+$").unwrap();
        let c = Base16::new();
        let config = TokenCreationConfig::new(1, Some(8));

        assert!(re.is_match(&c.create_token(&config).unwrap()));
    }
}
