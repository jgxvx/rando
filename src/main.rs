use clap::Parser;
use colored::Colorize;
use rando::cli;

fn main() {
    let opts = cli::Cli::parse();

    if let Err(e) = rando::run(opts) {
        eprintln!("{}", e.to_string().red());
        std::process::exit(exitcode::SOFTWARE);
    }
}
