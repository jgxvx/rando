use anyhow::Result;

pub mod app;
pub mod cli;
pub mod token;

pub fn run(cli_opts: cli::Cli) -> Result<()> {
    app::Application::new(cli_opts).run()
}
