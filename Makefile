.PHONY: install
install:
	cargo install --path .

.PHONY: test-image
test-image:
	docker build -t registry.gitlab.com/jgxvx/rando/test:latest -f devops/ci/images/test/Dockerfile .